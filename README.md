# ada-lelivre

Dépôt de la version web de l'ouvrage Ada & Zangemann de Matthias Kirschner et Sandra Brandstätter dans son édition française par C&F éditions.

Le site est visible à l'adresse : https://ada-lelivre.fr/

Licence Creative Commons : Attribution, Partage dans les mêmes conditions (CC BY-SA 4.0 FR). 

Les informations de licence sont dans la page colophon.html


## Traduction

Si vous réalisez une traduction, merci de la signaler ici, cela permettrait de réaliser et d'intégrer une version multilingue du site.


contact : editorial@cfeditions.com

Avec le soutien de la Direction du numérique pour l’éducation du ministère de l’Éducation nationale et de la Jeunesse.

*